import PSMSFramework
import time

caller_wdt = 0

def runner(w):
    global caller_wdt

    #print('[Info] Runner loop called')

    #Count timeout
    caller_wdt += 1

    #Call the function every 10 seconds only
    if caller_wdt == 20:
        caller_wdt = 0
        print('[Info] Run user function')
        #PSMSFramework.sysapi.run('test_example','gpio_ex_tester', {})

    time.sleep(0.5)

if __name__ == "__main__":
    PSMSFramework.PSMSFramework(
        title='<b>TEMPLATE</b> Tester',
        git_root='../',
        runner=runner,

        db_host='dbod-psms.cern.ch',
        db_username='admin',
        db_port=6600,
        db_password='psmspasswd#',
        db_dbname='psms'
    )
