"""The 'Test function Name' emulates an interface test.

The 'Test function Name' check 5 interfaces using the 'Debug function Name' subtest.
The flowchart below describe in details the test procedure.

.. graphviz::

    digraph G {
        start [
            label = "Start";
            shape = oval;
        ];

        continueTestint [
            label = "Intf. id < 5?";
            shape = diamond;
        ];

        runsubtest [
            label = "Run function_dbg(id, 0, 100)";
            shape = rect;
        ];

        isIOFalse [
            label = "Check subtest status";
            shape = diamond;
        ];

        testFails [
            label = "Set general test as failed";
            shape = rect;
        ];

        wait [
            label = "Wait 0.1s";
            shape = rect;
        ];

        end [
            label = "End";
            shape = oval;
        ];



        start -> continueTestint;

        continueTestint:s -> runsubtest:n [label = "Yes"];
        continueTestint -> end [label = "No"];

        runsubtest -> isIOFalse
        isIOFalse -> testFails [label = "Fail"]
        testFails:s -> wait:e
        isIOFalse -> wait [label = "Pass"]
        wait:w -> continueTestint:w

        {
            rank=same;
            continueTestint; end;
        }
        {
            rank=same;
            isIOFalse; testFails;
        }
    }

When finished, the test returns a dictionnary formated to be compliant with the
psms-tester framework. Then, the result is **displayed in the user's browser** according
to the display settings and **pushed into the database**.
"""

import PSMSFramework
import random
import getpass
import time

@PSMSFramework.sysapi.register_debug('Debug function Name')
def function_dbg(intfId, minvalue, maxvalue):
    '''
    Debug function description:

    As it is the case on the top comment for the test, it is a good method
    to describe the debug function here. This description will be print with
    the debug function result. It also support the graphviz extension as
    mentioned before. In addition, it is recommended to describe the function
    parameters and the return value.

    For example, this function generate 100 random values, display them in
    the console and returns the subtest result.

    Args:
        :intfId: interface identifier
        :minvalue: minimum value for random integer
        :maxvalue: maximum value for random integer

    Returns:
        This function returns the parameters set in input as an example:

        .. code-block:: javascript

            {
                'test_type_name': 'FN_DBG_EX',
                'name': 'fn_dbg_ex_{}'.format(intfId),

                'details': {
                    'intfId': intfId,
                    'minvalue': minvalue,
                    'maxvalue': maxvalue
                },

                'summary': {
                    'avg':(sum(randArr)/len(randArr)),
                    'max': max(randArr),
                    'min': min(randArr)
                },

                'measurements': meas,
                'pass': 0
            }
    '''

    #Print a message in the console
    print('[info] Debug function called')

    PSMSFramework.sysapi.get_yesno("Ready?",'''Please set the minimum potentiometer value (clockwise) and press enter''')

    #Measurement points variable
    meas = []
    randArr = []

    #Emulate a measurement
    for i in range(100):
        v = random.randint(int(minvalue),int(maxvalue))
        randArr.append(v)

        #Print the random value
        print('[meas] Random value ({}): {}'.format(i, v))

        #Store formatted measurement
        meas.append({
                'name': 'random_val',
                'data': {'v': v},
                'pass': 0
            })

    #Returns the subtest result
    return {
            'test_type_name': 'FN_DBG_EX',
            'name': 'fn_dbg_ex_{}'.format(intfId),

            'details': {
                'intfId': intfId,
                'minvalue': minvalue,
                'maxvalue': maxvalue
            },

            'summary': {
                'avg': (sum(randArr)/len(randArr)),
                'max': max(randArr),
                'min': min(randArr)
            },

            'measurements': meas,
            'pass': 0
        }

@PSMSFramework.sysapi.register_test('Test function Name')
def function_test():
    '''
    Test function description:

    This comment won't be used for the report but can be used to described
    the function in order to generate a sphinx documentation.

    Returns:
        This function returns the test result as needed by the framework:

        .. code-block:: javascript

            {
                'pass': 0,
                'details': {
                    'operator': getpass.getuser()
                },

                'device': {
                    'name': 'ex_device',
                    'details': {
                        'build_number': 'P20200001',
                        'serial_number': '0000 0001',
                    }
                },

                'subtests': subtests,

                'display': [
                        {
                            'test_type_name': 'FN_DBG_EX',
                            'summary': {
                                'type': 'table', #Only table is supported for subtest info display
                                'columns': [
                                    {'col': 'details', 'key': 'intfId', 'verbose': 'Interface ID'},
                                    {'col': 'details', 'key': 'minvalue', 'verbose': 'Min value setting'},
                                    {'col': 'details', 'key': 'maxvalue', 'verbose': 'Max value setting'},
                                    {'col': 'summary', 'key': 'avg', 'verbose': 'Average value meas.'},
                                    {'col': 'summary', 'key': 'min', 'verbose': 'Min value meas.'},
                                    {'col': 'summary', 'key': 'max', 'verbose': 'Max value meas.'}
                                ]
                            },
                            'measurements': [
                                {
                                    'type': 'scatter',
                                    'label': 'random value evolution',
                                    'title': 'Random values',
                                    'doc': 'Random values generated during the test',
                                    'point_names': 'random_val',
                                    'ykey': 'v'
                                }
                            ],
                            'doc': 'Optional - you can explain here what does the test do. It will be used into the report :-)'
                        }
                    ]
            }
    '''

    #Print a message in the console
    print('[info] Test function called')

    #Subtest results variable
    subtests = []
    global_pass = 0

    #Emulate a chip with 5 interfaces to be tested
    for i in range(5):
        s = function_dbg(i, 0, 100)
        subtests.append(s)

        if s['pass'] != 0:
            global_pass |= s['pass']

        time.sleep(0.1)

    #Returns the test result
    return {
                'pass': 0,
                'details': {
                    'operator': getpass.getuser()
                },

                'device': {
                    'name': 'ex_device',
                    'details': {
                        'build_number': 'P20200001',
                        'serial_number': '0000 0001',
                    },
                    'batch': {
                        'name': 'FAKEModule',
                        'details': {
                            'bn': 'P20200001'
                        }
                    }
                },

                'subtests': subtests,

                'display': [
                        {
                            'test_type_name': 'FN_DBG_EX',
                            'summary': {
                                'type': 'table', #Only table is supported for subtest info display
                                'columns': [
                                    {'col': 'details', 'key': 'intfId', 'verbose': 'Interface ID'},
                                    {'col': 'details', 'key': 'minvalue', 'verbose': 'Min value setting'},
                                    {'col': 'details', 'key': 'maxvalue', 'verbose': 'Max value setting'},
                                    {'col': 'summary', 'key': 'avg', 'verbose': 'Average value meas.'},
                                    {'col': 'summary', 'key': 'min', 'verbose': 'Min value meas.'},
                                    {'col': 'summary', 'key': 'max', 'verbose': 'Max value meas.'}
                                ]
                            },
                            'measurements': [
                                {
                                    'type': 'scatter',
                                    'label': 'random value evolution',
                                    'title': 'Random values',
                                    'doc': 'Random values generated during the test',
                                    'point_names': 'random_val',
                                    'ykey': 'v'
                                }
                            ],
                            'doc': 'Optional - you can explain here what does the test do. It will be used into the report :-)'
                        }
                    ]
            }