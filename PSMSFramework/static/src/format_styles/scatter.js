function format_scatter(id, disp, data){

    console.log(data);

    var chartData = [];

    var xkey = disp['xkey'];
    var ykey = disp['ykey'];

    var title = '';
    if('title' in disp)    title = disp['title'];
    var doc = '';
    if('doc' in disp)    doc = disp['doc'];
    var label = '';
    if('label' in disp)    label = disp['label'];

    var xpos = 0;
    $.each(data, function(key, d){
        if(xkey in d){
            chartData.push({x: d[xkey], y: d[ykey]});
        }else{
            chartData.push({x: xpos, y: d[ykey]});
        }
        xpos += 1;
    });

    var html_content = ''+
        '<div>'+
        '<h4>'+title+'</h4>'+
        '<div class="w3-container">'+
        '<p style="margin: 0; margin-bottom: 16px">'+doc+'</p>'+
        '<canvas id="scatter-meas-'+id+'" data-label="'+label+'" data-title="'+title+'" data-chart=\''+JSON.stringify(chartData)+'\' >'+
        '</canvas>'+
        '</div>'+
        '</div>';

    return {
        'html': html_content,
        'id': 'scatter-meas-'+id,
        'js': function(id){

            var d = JSON.parse($('#'+id).attr('data-chart'));
            var label = $('#'+id).attr('data-label');
            var title = $('#'+id).attr('data-title');

            var config = {
                type: 'scatter',
                data: {
                    datasets: [{
                        label: label,
                        backgroundColor: '#009688',
                        borderColor: '#009688',
                        data: d,
                        fill: false,
                        showLine: true,
                        pointBorderWidth: 1,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: false,
                        text: title
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    scales: {
                        x: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        },
                        y: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }
                    }
                }
            };

            var ctx = document.getElementById(id).getContext('2d');
            window.myLine = new Chart(ctx, config);

        }
    }
}