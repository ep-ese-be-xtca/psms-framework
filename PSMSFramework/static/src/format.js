(function( $ ){

    var waitForEl = function(selector, callback) {
        if (jQuery(selector).length) {
            //callback();
            setTimeout(function() {
              callback();
            }, 500);
          } else {
            setTimeout(function() {
              waitForEl(selector, callback);
            }, 100);
          }
    };

    $.fn.viewmeas = function() {
        var meas = JSON.parse($(this).attr('data-meas'));
        var disp = JSON.parse($(this).attr('data-disp'));

        var show_meas_node = '';
        var cbacks = [];

        var id = 0;

        $.each(disp, function(key, disp_style){

            var meas_name = disp_style['point_names'];

            var d = [];

            $.each(meas, function(key, meas_point){
                console.log('meas_point["name"]: '+meas_point['name']+' / meas_name: '+ meas_name);

                if(meas_point['name'] != meas_name){
                    console.log('different names');
                    return;
                }

                console.log('push to D');
                d.push(meas_point['data']);
            });

            console.log('d.length() -> '+d.length);

            if(d.length > 0){
                tmp = format(id, disp_style['type'], disp_style, d);
                show_meas_node += tmp['html'];
                cbacks.push({'func': tmp['js'], 'id': tmp['id']});
            }

            id += 1;
        });

        $('#meas-disp-node').html(show_meas_node);
        $.each(cbacks, function(k, v){
            v['func'](v['id']);
        });
        $('#meas-disp-modal').fadeIn(200);
        /*
        $('#meas-disp-modal').fadeIn(200, function(){
            $('meas-disp-node > .table-v').each(function(){
                var maxwidth = 0;

                $('.fix', this).each(function(){
                if($(this).width() > maxwidth){
                    maxwidth = $(this).width();
                }
              });

              maxwidth += 20;
              console.log(maxwidth);

              $('.fix', this).css('margin-left', '-'+(maxwidth+20)+'px');
              $('.fix', this).css('width', ''+maxwidth+'px');
              $('.inner', this).css('margin-left', ''+maxwidth+'px');


              var innerW = $('.inner', this).width();
              $('table', this).css('width', ''+innerW+'px');

              $('.inner', this).css('display', 'block');
            });
        });
        */
    }

    $.fn.format_test = function(data) {

        var show_result_node = '<h3>'+data['name']+'</h3>'

        /** Pass/Fail banneer */
        if (data['ret']['pass'] == 0){
            show_result_node += '<div style="text-align: justify" class="w3-green w3-container w3-card"><p><b>The test passed successfully</b></p></div>';
        } else {
            show_result_node += '';

            show_result_node += '<div style="text-align: justify" class="w3-red w3-container w3-card">';
            show_result_node += '    <p style=><b>The test failed with the following code: '+data['ret']['pass']+'</b></p>';
            show_result_node += '    <ul style="list-style: none;">';
            $.each(data['ret']['subtests'], function(key, value){
                if(value['pass'] != 0){
                    show_result_node += '<li>';
                    show_result_node += '<b>'+value['name']+'</b> failed with the following code: '+value['pass'];
                    show_result_node += '</li>';
                }
            });
            show_result_node += '     </ul>';
            show_result_node += '</div>';
        }

        /** Documentation */
        show_result_node += '<h3>Description</h3>'
        show_result_node += '<p>'+data['doc']+'</p>'

        show_result_node += '<div class="w3-row"><div class="w3-col l6"><h4>Details:</h4><ul>'
        $.each(data['ret']['details'], function(index, value){
            show_result_node += '<li><b>'+index+'</b>: '+value+'</li>';
        });
        show_result_node += '</ul></div><div class="w3-col l6"><h4>Device ('+data['ret']['device']['name']+'):</h4><ul>';
        $.each(data['ret']['device']['details'], function(index, value){
            show_result_node += '<li><b>'+index+'</b>: '+value+'</li>';
        });
        show_result_node += '</ul></div></div>';

        /** Subtests */
        show_result_node += '<h3>Subtests</h3>';
        show_result_node += '<div class="w3-row w3-container">';

        $.each(data['ret']['display'], function(key, value){

            var test_type = value['test_type_name'];
            var summary_display = value['summary'];

            show_result_node += '<h5>'+value['title']+'</h5>';

            show_result_node += '<div class="w3-container">';

            if('doc' in value){
                show_result_node += '<p>'+value['doc']+'</p>';
            }

            //Subtest can only be displayed as table!!

            //if(summary_display['type'] == 'table'){

                show_result_node += '<div class="table-v">';
                show_result_node += '    <div class="outer">';
                show_result_node += '        <div class="inner"><table>';
                show_result_node += '            <thead><tr>';
                show_result_node += '            <th style="text-align: center; padding-left:16px; padding-right: 16px" class="fix">Name</th>';
                $.each(summary_display['columns'], function(k,v){
                    show_result_node += '        <th style="text-align: center; padding-left:16px; padding-right: 16px">'+v['verbose']+'</th>';
                });

                if('measurements' in value){
                    show_result_node += '            <th style="text-align: center; padding-left:16px; padding-right: 16px">Measurements</th>';
                }
                show_result_node += '            </tr></thead>';
                show_result_node += '            <tbody>';

                $.each(data['ret']['subtests'], function(key, sbvalue){

                    if(sbvalue['test_type_name'] != test_type){
                        return;
                    }

                    show_result_node += '        <tr>';
                    show_result_node += '        <td style="text-align: center; padding-left:16px; padding-right: 16px" class="fix">'+sbvalue['name']+'</td>';

                    $.each(summary_display['columns'], function(k,v){
                        show_result_node += '    <td style="text-align: center; padding-left:16px; padding-right: 16px">'+sbvalue[v['col']][v['key']]+'</td>';
                    });
                    if('measurements' in value){
                        show_result_node += '        <td onclick="$(this).viewmeas()" data-meas=\''+JSON.stringify(sbvalue['measurements'])+'\' data-disp=\''+JSON.stringify(value['measurements'])+'\' style="text-align: center; padding-left:16px; padding-right: 16px; cursor: pointer" class="w3-hover-text-teal" >View</td>';
                    }

                    show_result_node += '        </tr>';
                });
                show_result_node += '</tbody></table></div></div></div>';
                show_result_node += '</div>';
            //}

        });

        show_result_node += '</div>';

        //''+
        //'<p>During this test, the following test were performed:</p>'+
        //'<ul>'+
        //'    <li><b>Title</b>: value</li>'+
        //'</ul>'


        $(this).html(show_result_node);

        waitForEl(this, function() {
            $('.table-v').each(function(){
              var maxwidth = 0;

              /*$('.fix', this).each(function(){

                if($(this).width() > maxwidth){
                    maxwidth = $(this).width();
                }
              });*/

              var textlen = 0;
                $('.fix', this).each(function(){
                    if ($(this).text().length > textlen){
                        textlen = $(this).text().length;
                    }
              });

              maxwidth = textlen*10;

              maxwidth += 20;
              console.log(maxwidth);

              $('.fix', this).css('margin-left', '-'+(maxwidth+20)+'px');
              $('.fix', this).css('width', ''+maxwidth+'px');
              $('.inner', this).css('margin-left', ''+maxwidth+'px');


              //var innerW = $('.inner', this).width();
              //$('table', this).css('width', ''+innerW+'px');

              $('.inner', this).css('display', 'block');
              //$('.fix', this).css('width'. maxwidth+'px');
              //$('.fix', this).css('margin-left'. inv+'px');
              //$('.inner', this).css('margin-left'. maxwidth+'px');
            });
        });

      return this;
   };
})( jQuery );