import threading
import sys
import signal
import time
import traceback
import os

from flask import Flask
from gevent import pywsgi

import PSMSFramework.WebServer as WebServer
import PSMSFramework.Worker as Worker
import PSMSFramework.sysapi as sysapi

class ServiceExit(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass

class PSMSFramework:

    def __init__(self, title, runner=None, git_root='.', tests_root='.', export_root='', port=8000, db_host=None, db_username=None, db_password=None, db_port=None, db_dbname=None):

        if export_root and not os.path.exists(export_root):
            raise Exception('Exported root path "{}" does not exists'.format(export_root))

        # Register the signal handlers
        signal.signal(signal.SIGTERM, self.service_shutdown)
        signal.signal(signal.SIGINT, self.service_shutdown)

        if db_host is None:
            print('[Info] Database interface is not set')
        else:
            if db_username is None:
                print('[Error] Database interface is missing db_username info')
            if db_password is None:
                print('[Error] Database interface is missing db_password info')
            if db_port is None:
                print('[Error] Database interface is missing db_port info')
            if db_dbname is None:
                print('[Error] Database interface is missing db_dbname info')

        #Start services
        try:

            self.worker = Worker.Worker(host=db_host, username=db_username, password=db_password, port=db_port, dbname=db_dbname)

            sysapi.CONFIG_OBJ.set_worker_class(self.worker)

            self.flaskThread = WebServer.WebServer(title, self.worker, git_root, tests_root, export_root, port)
            self.flaskThread.start()

            while True:
                if(self.worker.isOff()):
                    print('[Info] Shutdown - server will be stopped in 1 second, allowing browser to recover all of the test data')
                    time.sleep(1)
                    self.flaskThread.kill()
                    break

                self.worker.run()

                if runner is not None:
                    try:
                        runner(self.worker)
                    except Exception as e:
                        if type(e).__name__ == 'ServiceExit':
                            self.flaskThread.kill()
                            break
                        else:
                            print(traceback.format_exc())
                            print('[Info] Exception handled with care, server continue running')

            self.flaskThread.join()

            if self.flaskThread.err is not None:
                raise Exception(self.flaskThread.err)

        except ServiceExit:
            self.flaskThread.kill()
            self.flaskThread.join()

    def service_shutdown(self, signum, frame):
        raise ServiceExit
