#-*- coding: utf-8 -*-
""" TestResTBLib is made to fill-up the test result database in automatic testbenches  """

import psycopg2
import numbers
import json
import datetime
from io import StringIO

class TestResException(Exception):
    """ Custom exception class.

    Attributes:
        msg  -- explanation of the error
    """

    def __init__(self, msg):
        """ Simple constructor """
        Exception.__init__(self, msg)
        self.msg = msg

    def __str__(self):
        """ Returns error message """
        return self.msg


class PSQLWriter:

    SUCCESS = 1
    FAILED = 0
    NA = -1

    ''' Init: call the connect function '''
    def __init__(self,host,username,password,port,dbname):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.dbname = dbname

        self.connect()
        self.cur = self.conn.cursor()
        self.conn.autocommit = True

        '''
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_pkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_extra_id_fkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_setting_id_fkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_dev_id_fkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_test_type_id_fkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_subtest_id_fkey;', None)
        except:
            pass
        try:
            self.send('ALTER TABLE MeasurementInstances DROP CONSTRAINT measurementinstances_test_id_fkey;', None)
        except:
            pass
        '''

    ''' Disconnect '''
    def disconnect(self):
        if self.conn is None:
            return

        try:
            self.cur.close()
        except:
            pass

        self.conn.close()
        self.conn = None

    ''' Connect to the database '''
    def connect(self):
        try:
            self.conn = psycopg2.connect(host=self.host,
                port=self.port,
                user=self.username,
                password=self.password,
                dbname=self.dbname )

            if self.conn == -1:
                self.conn = None
                raise TestResException("DB connection error")

        except Exception as e:
            self.conn = None
            raise TestResException(str(e))

    ''' Execute SQL with response '''
    def query(self, sqlcommand, params=None):
        if(self.conn == None):
            raise TestResException('Not connected')

        try:
            cur = self.conn.cursor()
            cur.execute( sqlcommand, params )
        except psycopg2.Error as e:
            raise TestResException(str(e.pgerror))

        resp = cur.fetchall()
        return resp

    ''' Execute SQL without response '''
    def send(self, sqlcommand, params):
        if(self.conn == None):
            raise TestResException('Not connected')

        try:
            self.cur.execute( sqlcommand, params)

        except psycopg2.Error as e:
            raise TestResException(str(e.pgerror))

    ''' DEVICE FUNCTIONS:
            -> getDevice: allow getting a device node (id and description)
            -> addDevice: allow adding a device in the list
    '''

    def getBatch(self, batchId = -1, batchName = None, batchDesc = None):
        params = None

        sqlRequest = 'SELECT * FROM Batches WHERE '
        sqlFirstParam = True

        if batchId >= 0:
            sqlRequest += ' Batch_id = {:d}'.format(deviceId)
            sqlFirstParam = False

        if batchName is not None:
            if sqlFirstParam == False:
                sqlRequest += " AND "
            sqlRequest += " Name = '{}'".format(batchName)
            sqlFirstParam = False

        if batchDesc is not None:
            params = []

            for key in batchDesc.keys():
                if sqlFirstParam == False:
                    sqlRequest += " AND "

                if isinstance(batchDesc[key],numbers.Number):
                    sqlRequest += " CAST(details->>'{}' as {}) = %s".format(key, type(batchDesc[key]).__name__)
                else:
                    sqlRequest += " details->>'{}' = %s".format(key)

                params.append(batchDesc[key])
                sqlFirstParam = False

        else:
            raise TestResException('Search parameters were not set (batchId = {}, batchName = {} and batchDesc = {})'.format(batchId, batchName, batchDesc))

        resp = self.query(sqlRequest, params)
        if len(resp) <= 0:
            raise TestResException('No batch found for parameters (batchId = {}, batchName = {} and batchDesc = {})'.format(batchId, batchName, batchDesc))

        ret = []
        for r in resp:
            obj = {
                'batchId' : r[0],
                'batchDate': r[1],
                'batchQty': r[2],
                'batchDetails': r[3]
            }
            ret.append(obj)

        return ret

    def getDevice(self, deviceId = -1, deviceName = '', batchId = None, deviceDesc = None):
        params = None

        sqlRequest = 'SELECT * FROM devices WHERE '
        sqlFirstParam = True

        if deviceId >= 0:
            sqlRequest += ' dev_id = {:d}'.format(deviceId)
            sqlFirstParam = False

        if deviceName != '':
            if sqlFirstParam == False:
                sqlRequest += " AND "
            sqlRequest += " name = '{}'".format(deviceName)
            sqlFirstParam = False

        if deviceDesc != None:
            params = []

            i = 0
            for key in deviceDesc.keys():
                if sqlFirstParam == False:
                    sqlRequest += " AND "

                if isinstance(deviceDesc[key],numbers.Number):
                    sqlRequest += " CAST(details->>'{}' as {}) = %s".format(key, type(deviceDesc[key]).__name__)
                else:
                    sqlRequest += " details->>'{}' = %s".format(key)
                params.append(deviceDesc[key])
                sqlFirstParam = False

        if batchId != None:
            if sqlFirstParam == False:
                sqlRequest += " AND "
            sqlRequest += " Batch_id = {:d}".format(batchId)
            sqlFirstParam = False

        if deviceId < 0 and deviceName == '' and batchId is None and deviceDesc is None:
            raise TestResException('Search parameters were not set (deviceId = {}, deviceDesc = {})'.format(deviceId, deviceDesc))

        resp = self.query(sqlRequest, params)
        if len(resp) <= 0:
            raise TestResException('No device found')

        ret = []
        for r in resp:
            obj = {
                'deviceId' : r[0],
                'deviceBatchId': r[1],
                'deviceUserId': r[2],
                'deviceReqMgtId': r[3],
                'deviceName' : r[4],
                'deviceDetails': r[5]
            }
            ret.append(obj)

        return ret

    def addDevice(self, deviceName = '', batchId = None, deviceDesc = None):
        if deviceDesc == None:
            raise TestResException('Device description shall contain at least one parameter (deviceDesc = {})'.format(deviceDesc))
        if batchId == None:
            raise TestResException('Batch id shall contain a valid number (batchId = {})'.format(batchId))

        sqlRequest = "INSERT INTO devices (name, details, batch_id) VALUES('{}','{}', {}) RETURNING dev_id".format(deviceName, json.dumps(deviceDesc), batchId)
        r = self.query(sqlRequest, None)

        return r[0][0]

    def addTestType(self, testTypeName = '', testTypeDesc = None):
        if testTypeDesc == None:
            raise TestResException('TestType description shall contain at least one parameter (testTypeDesc = {})'.format(testTypeDesc))
        if testTypeName == '':
            raise TestResException('testTypeName cannot be empty (testTypeName = {})'.format(testTypeName))
        if len(testTypeName) > 32:
            raise TestResException('testTypeName cannot be longer than 32 characters (testTypeName = {})'.format(testTypeName))

        sqlRequest = "INSERT INTO testtypes (name, details) VALUES( '{}','{}') RETURNING testtype_id".format(testTypeName, json.dumps(testTypeDesc))
        r = self.query(sqlRequest, None)

        return r[0][0]

    def getTestType(self, testTypeId = -1, testTypeName = '', testTypeDesc = None):
        params = None

        sqlRequest = 'SELECT * FROM testtypes WHERE '
        sqlFirstParam = True

        if testTypeId >= 0:
            sqlRequest += ' testtype_id = {:d}'.format(testTypeId)
            sqlFirstParam = False

        if testTypeName != '':
            if sqlFirstParam == False:
                sqlRequest += " AND "
            sqlRequest += ' name = \'{}\''.format(testTypeName)
            sqlFirstParam = False

        if testTypeDesc != None:
            params = []

            for key in testTypeDesc.keys():
                if sqlFirstParam == False:
                    sqlRequest += " AND "
                if isinstance(testTypeDesc[key],numbers.Number):
                    sqlRequest += " CAST(details->>'{}' as {}) = %s".format(key, type(testTypeDesc[key]).__name__)
                else:
                    sqlRequest += " details->>'{}' = %s".format(key)
                params.append(testTypeDesc[key])
                sqlFirstParam = False

        if sqlFirstParam:
            sqlRequest = 'SELECT * FROM testtypes'

        resp = self.query(sqlRequest, params)

        if len(resp) <= 0:
            raise TestResException('No test type found for params (testTypeName: {}, testTypeDesc: {})'.format(testTypeName, testTypeDesc))

        ret = []
        for r in resp:
            obj = {
                'testTypeId' : r[0],
                'testTypeName' : r[1],
                'testTypeDetails': r[2]
            }
            ret.append(obj)

        return ret

    def addTest(self, deviceId = None, testDescription = None, passed=-1):
        if testDescription == None:
            raise TestResException('TestInstance description shall contain at least one parameter (testDescription = {})'.format(testDescription))
        if deviceId == None:
            raise TestResException('deviceId shall be defined (deviceId = {})'.format(deviceId))

        sqlRequest = "INSERT INTO testinstances (Dev_id, date, details, summary, pass) VALUES({:d}, NOW(),'{}','{}', {}) RETURNING test_id".format(deviceId, json.dumps(testDescription), json.dumps({}), passed)
        r = self.query(sqlRequest, None)

        return r[0][0]

    def addSubTest(self, testId = None, testTypeId = None, subTestName = '', subTestSum = None, subTestDetails = None, passed = -1):
        if subTestSum == None:
            raise TestResException('SubTest description shall contain at least one parameter (subTestSum = {})'.format(subTestSum))
        if subTestDetails == None:
            raise TestResException('SubTest description shall contain at least one parameter (subTestDetails = {})'.format(subTestDetails))
        if testId == None:
            raise TestResException('TestId shall be defined (testId = {})'.format(testId))
        if testTypeId == None:
            raise TestResException('testTypeId shall be defined (testTypeId = {})'.format(testTypeId))

        sqlRequest = "INSERT INTO subtestinstances (Test_id, TestType_id, name, summary, details, pass) VALUES({:d}, {:d}, '{}','{}', '{}', {}) RETURNING subtest_id".format(
            testId, testTypeId, subTestName, json.dumps(subTestSum), json.dumps(subTestDetails), passed)
        r = self.query(sqlRequest, None)

        return r[0][0]

    def pushSubTest(self, testId = None, testTypeId = None, deviceId = None, name = '', summary = None, details = None, measurements = None, passed = -1):
        if testId == None:
            raise TestResException('testId shall be defined (testId = {})'.format(testId))
        if testTypeId == None:
            raise TestResException('testTypeId shall be defined (testTypeId = {})'.format(testTypeId))
        if deviceId == None:
            raise TestResException('deviceId shall be defined (deviceId = {})'.format(deviceId))
        if summary == None:
            raise TestResException('summary shall be defined (summary = {})'.format(summary))
        if measurements == None:
            raise TestResException('measurements shall be defined (measurements = {})'.format(measurements))

        # Add subtest
        subtestId = self.addSubTest(testId = testId,
                                    testTypeId = testTypeId,
                                    subTestName = name,
                                    subTestSum = summary,
                                    subTestDetails = details,
                                    passed = passed)

        measdata = StringIO()
        measdata.truncate(0)
        measdata.seek(0)

        for meas in measurements:
            if 'passed' in meas:
                passed = meas['passed']
            else:
                passed = -1

            measdata.write("{:d};{:d};{:d};{:d};{};{};{}\n".format(testId, deviceId, subtestId, testTypeId, meas['name'], json.dumps(meas['data']), passed))

        col=('test_id', 'dev_id', 'subtest_id', 'testtype_id', 'name', 'datapoint', 'pass')
        measdata.seek(0)
        self.cur.copy_from(measdata, 'MeasurementInstances', columns=col, sep=';')


    ''' Additional tools: '''
    def getDBSize(self):
        r = self.query('SELECT pg_size_pretty(pg_database_size(current_database())) as size');
        return r[0][0]


