"""The '[Example GPIO] tester' check the GPIO feature.

This test emulate a full test of a GPIO. In this case, each I/O is tested in a subtest instance
in order to measure the maximum output current in output mode and the voltage thresholds in
input. In addition, it checks the output voltage when the pin is set to low.

.. graphviz::

    digraph G {
        node [fontname = "Handlee"];
        edge [fontname = "Handlee"];

        start [
            label = "Start";
            shape = oval;
        ];

        askDevice [
            label = "Ask for device ID";
            shape = rect;
        ];

        continueTestint [
            label = "I/O id < 8?";
            shape = diamond;
        ];

        runsubtest [
            label = "Run gpio_subtest(id)";
            shape = rect;
        ];

        isIOFalse [
            label = "Check subtest status";
            shape = diamond;
        ];

        testFails [
            label = "Set general test as failed";
            shape = rect;
        ];

        wait [
            label = "Set progress and wait 0.5s";
            shape = rect;
        ];

        end [
            label = "End";
            shape = oval;
        ];



        start -> askDevice;
        askDevice:s -> continueTestint:n;

        continueTestint:s -> runsubtest:n [label = "Yes"];
        continueTestint -> end [label = "No"];

        runsubtest -> isIOFalse
        isIOFalse -> testFails [label = "Fail"]
        testFails:s -> wait:e
        isIOFalse -> wait [label = "Pass"]
        wait:w -> continueTestint:w

        {
            rank=same;
            continueTestint; end;
        }
        {
            rank=same;
            isIOFalse; testFails;
        }
    }

When finished, the test returns a dictionnary formated to be compliant with the
psms-tester framework. The result is then **displayed in the user's browser** according
to the display setting and **pushed into the database**.
"""

import random
import time
import numpy as np
import getpass

import PSMSFramework

MIN_HIGH_DET_FAILURE = 0x01
MAX_ZERO_DET_FAILURE = 0x02
MIN_HIGH_SET_FAILURE = 0x04
MAX_ZERO_SET_FAILURE = 0x08

def get_gpio_state(input_v):
    """Emulate a GPIO read action

    This function evaluates the GPIO value depending on the input voltage of
    the pin. To emulate errors, the pin value is randomized with probability
    factors depending on the input voltage.

    Args:
        input_v: input voltage.

    Returns:
        Returns the pin state (0: low / 1: high)
    """
    if input_v < 0.5:
        return int(np.random.choice(np.array([0, 1]), p=[0.95, 0.05]))
    elif input_v < 0.8:
        return int(np.random.choice(np.array([0, 1]), p=[0.5, 0.5]))
    else:
        return int(np.random.choice(np.array([0, 1]), p=[0.05, 0.95]))

def get_gpio_output_voltage(output_l):
    """Emulate a GPIO set action

    This functions evaluates a measured voltage on the output of the pin. In
    order to accurate the emulation, the voltage output depends on the load.
    In addition, to simulate wrong behaviours, the output voltage is randomized
    using probability factors debending on the output load.

    Args:
        output_l: output load (in ohms).

    Returns:
        Returns the output voltage that could be measured at the output of the pin.
    """
    if output_l < 3:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.50, 0.20, 0.15, 0.06, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01]
            )
        )
    elif output_l < 3.5:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.01, 0.01, 0.01, 0.01, 0.01, 0.07, 0.10, 0.50, 0.10, 0.10, 0.06, 0.01, 0.01]
            )
        )
    else:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.06, 0.15, 0.20, 0.50]
            )
        )

@PSMSFramework.sysapi.register_debug('GPIO Subtest')
def gpio_subtest(io_id, input_min_voltage, input_max_voltage, input_steps, output_min_load, output_max_load, output_steps, high_voltage_threshold):
    """Test a specific I/O pin

    This function test a specific GPIO depending on different parameters.
    The flowchart below describe the testing procedure:

    .. graphviz::

        digraph G {
            node [fontname = "Handlee"];
            edge [fontname = "Handlee"];

            start [
                label = "Start";
                shape = oval;
            ];

            init_inputv [
                label = "inputV = input_min_voltage";
                shape = rect;
            ];

            inputv_inrange [
                label = "inputV";
                shape = diamond;
            ];

            get_gpio_state [
                label = "Set input voltage and get GPIO state";
                shape = rect;
            ];

            is_iohigh [
                label = "I/O = 1";
                shape = diamond;
            ];

            is_minhigh_det [
                label = "min_high_det not set";
                shape = diamond;
            ];

            set_minhigh_det [
                label = "min_high_detect = inputV";
                shape = rect;
            ];

            set_maxzer_det [
                label = "max_zero_detect = inputV";
                shape = rect;
            ];

            inc_inputv [
                label = "inputV += input_steps";
                shape = rect;
            ];

            init_outputl [
                label = "outputL = output_min_load";
                shape = rect;
            ];

            outputl_inrange [
                label = "outputL";
                shape = diamond;
            ];

            get_gpio_outvoltage [
                label = "Set load and get GPIO output voltage";
                shape = rect;
            ];

            is_ouputv [
                label = "output_v?";
                shape = diamond;
            ];

            set_minhigh_set [
                label = "min_high_set = output_v";
                shape = rect;
            ];

            is_minhight_set [
                label = "min_high_set not set";
                shape = diamond;
            ];

            set_maxzer_set [
                label = "max_zero_set = output_v";
                shape = rect;
            ];

            inc_outputl [
                label = "outputL += output_steps";
                shape = rect;
            ];

            end [
                label = "End";
                shape = oval;
            ];

            start -> init_inputv
            init_inputv -> inputv_inrange
            inputv_inrange:w -> get_gpio_state:n [label = "<= input_max_voltage"];
            get_gpio_state:s -> is_iohigh:n
            is_iohigh:e -> set_maxzer_det:w [label = "No"];
            is_iohigh:s -> is_minhigh_det:n [label = "Yes"];
            is_minhigh_det:s -> set_minhigh_det:n [label = "Yes"]
            is_minhigh_det:e -> inc_inputv:e [label = "No"]
            set_maxzer_det:s -> inc_inputv:e
            set_minhigh_det:s -> inc_inputv
            inc_inputv:s -> init_outputl:w
            inputv_inrange:e -> init_outputl:n [label = "else"];
            init_outputl -> outputl_inrange
            outputl_inrange:w -> get_gpio_outvoltage:n [label = "<= output_max_load"];
            get_gpio_outvoltage:s -> is_ouputv:n
            is_ouputv:e -> set_maxzer_set:w [label = "< high_voltage_threshold"];
            is_ouputv:s -> is_minhight_set:n [label = ">= high_voltage_threshold"];
            is_minhight_set:s -> set_minhigh_set:n [label = "Yes"]
            is_minhight_set:e -> inc_outputl:e [label = "No"]
            set_maxzer_set:s -> inc_outputl:e
            set_minhigh_set:s -> inc_outputl
            outputl_inrange:e -> end:n [label = "else"];
            inc_outputl -> end

            {
                rank=same;
                is_iohigh; set_maxzer_det;
            }
            {
                rank=same;
                is_ouputv; set_maxzer_set;
            }
        }


    Args:
        io_id: specify the pin id to be tested
        input_min_voltage: minimum input voltage to be set during the input mode testing (in volt)
        input_max_voltage: maximum input voltage to be set during the input mode testing (in volt)
        input_steps: step size for the input mode testing (in volt)
        output_min_load: minimum output load to be set during the output mode testing (in ohm)
        output_max_load: maximum output load to be set during the output mode testing (in ohm)
        output_steps: step size for the output mode testing (in ohm)
        high_voltage_threshold: threshold voltage to consider the output to be high

    Returns:
        The gpio_subtest function returns all of the test details formatted like described below

        .. code-block:: javascript

            {
                'test_type_name': 'GPIO_EMUL_v0',
                'name': 'gpio_{}'.format(io_id),

                'details': {
                    'io_id': io_id,
                    'input_min_voltage': input_min_voltage,
                    'input_max_voltage': input_max_voltage,
                    'input_steps': input_steps,
                    'output_min_load': output_min_load,
                    'output_max_load': output_max_load,
                    'output_steps' : output_steps,
                    'high_voltage_threshold': high_voltage_threshold
                },

                'summary': {
                    'min_high_detect': MIN_HIGH_DETECT,
                    'max_zero_detect': MAX_ZERO_DETECT,
                    'min_high_set': MIN_HIGH_SET,
                    'max_zero_set': MAX_ZERO_SET,
                },

                'measurements': MEASUREMENTS,
                'pass': pass_v
            }
    """

    MEASUREMENTS = []
    MIN_HIGH_DETECT = -1
    MAX_ZERO_DETECT = -1
    MIN_HIGH_SET = -1
    MAX_ZERO_SET = -1

    #Check input:
    input_voltages = np.arange(float(input_min_voltage), float(input_max_voltage), float(input_steps)).tolist()

    for input_v in input_voltages:
        gpio_in = get_gpio_state(input_v)

        MEASUREMENTS.append({
            'name': 'input_voltage_detect',
            'data': {'v': input_v, 'r': gpio_in},
            'pass': -1
        })

        if gpio_in == 1 and MIN_HIGH_DETECT == -1:
            MIN_HIGH_DETECT = input_v

        if gpio_in == 0:
            MAX_ZERO_DETECT = input_v

    #Check output:
    output_loads = np.arange(float(output_min_load), float(output_max_load), float(output_steps)).tolist()

    for output_l in output_loads:
        output_v = get_gpio_output_voltage(output_l)

        MEASUREMENTS.append({
            'name': 'output_load_test',
            'data': {'l': output_l, 'v': output_v},
            'pass': -1
        })

        if output_v > float(high_voltage_threshold) and MIN_HIGH_SET == -1:
            MIN_HIGH_SET = output_v

        if output_v < float(high_voltage_threshold):
            MAX_ZERO_SET = output_v

    #Check pass/fail
    pass_v = 0x00

    if MIN_HIGH_DETECT < 0.5:
        pass_v |= MIN_HIGH_DET_FAILURE

    if MAX_ZERO_DETECT > 0.8:
        pass_v |= MAX_ZERO_DET_FAILURE

    if MIN_HIGH_SET < float(high_voltage_threshold):
        pass_v |= MIN_HIGH_SET_FAILURE

    if MAX_ZERO_SET >= float(high_voltage_threshold):
        pass_v |= MAX_ZERO_SET_FAILURE

    return {
            'test_type_name': 'GPIO_EMUL_v0',
            'name': 'gpio_{}'.format(io_id),

            'details': {
                'io_id': io_id,
                'input_min_voltage': float(input_min_voltage),
                'input_max_voltage': float(input_max_voltage),
                'input_steps': float(input_steps),
                'output_min_load': float(output_min_load),
                'output_max_load': float(output_max_load),
                'output_steps' : float(output_steps),
                'high_voltage_threshold': float(high_voltage_threshold)
            },

            'summary': {
                'min_high_detect': MIN_HIGH_DETECT,
                'max_zero_detect': MAX_ZERO_DETECT,
                'min_high_set': MIN_HIGH_SET,
                'max_zero_set': MAX_ZERO_SET,
            },

            'measurements': MEASUREMENTS,
            'pass': -pass_v
    }

@PSMSFramework.sysapi.register_test('Example [GPIO Emulation]')
def gpio_ex_tester():
    """
    A one line comment giving a really short description

    Then, after one blank line, the rest of the docstring should contain an
    overall description of the function content.

    Args:
        arg_name: contains an argument description that can be multi-line
            with an additional increment on the next line(s).

    Returns:
        This indented paragraph contains the return value description. In case
        a dictionnary is returned, this one can be documented as well like the
        following exemple:

        .. code-block:: javascript

            {
                'Device': DEVICE,

                'Test': {
                    'name': 'EMUL_GPIO_Test_v0',
                    'pass': global_res,
                    'details': {
                        'operator': getpass.getuser()
                    }
                },

                'Subtests': SUBTESTS,

                'display': [
                    {
                        'test_type_name': 'GPIO_EMUL_v0',
                        'summary': {
                            'type': 'table',
                            'columns': [
                                {'key': 'min_high_detect', 'verbose': 'Min. input voltage detected'},
                                {'key': 'max_zero_detect', 'verbose': 'Max. input voltage not detected'},
                                {'key': 'min_high_set', 'verbose': 'Min. output load supported'},
                                {'key': 'max_zero_set', 'verbose': 'Max. output load not supported'}
                            ]
                        }
                    },
                    {
                        'test_type_name': 'GPIO_EMUL_v0',
                        'measurements': {
                            'type': 'graph',
                            'point_names': 'input_voltage_detect',
                            'x': 'v',
                            'y': 'r'
                        }
                    },
                    {
                        'test_type_name': 'GPIO_EMUL_v0',
                        'measurements': {
                            'type': 'graph',
                            'point_names': 'output_load_test',
                            'x': 'l',
                            'y': 'v'
                        }
                    }
                ]
            }

    Raises:
        IOError: Description the error that can be raised!
    """

    #Get device details
    dev_sn = PSMSFramework.sysapi.get_data('Device BN/SN','''
        Please, inform what is the device under test. To do this you can eithe:
        <ul>
            <li>Scan the QR code located on the device sticker</li>
            <li>Enter the BN/SN value manually</li>
        </ul>
    ''')

    boardSplittedRef = dev_sn.rstrip().split("/")

    DEVICE = {
        'name': 'vldbplus_v0',
        'details': {
            'build_number': boardSplittedRef[0],
            'serial_number': boardSplittedRef[1],
        },
        'batch': {
            'name': 'FAKEModule',
            'details': {
                'bn': boardSplittedRef[0]
            }
        }
    }

    #Run subtests
    SUBTESTS = []
    global_res = 0x00

    for i in range(0, 8):

        s = gpio_subtest(
            io_id=i,
            input_min_voltage=0,
            input_max_voltage=1.2,
            input_steps=0.1,
            output_min_load=0,
            output_max_load=10,
            output_steps=0.1,
            high_voltage_threshold=0.8
        )

        PSMSFramework.sysapi.set_job_progress(10+(i*10))

        SUBTESTS.append(s)

        if s['pass'] != 0:
            global_res |= -s['pass']

        time.sleep(0.5)

    #get test details
    global_res = global_res * (-1)

    return {
        'pass': global_res,
        'details': {
            'operator': getpass.getuser()
        },
        'device': DEVICE,
        'subtests': SUBTESTS,

        'display': [
            {
                'test_type_name': 'GPIO_EMUL_v0',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'summary', 'key': 'min_high_detect', 'verbose': 'Min. input voltage detected'},
                        {'col': 'summary', 'key': 'max_zero_detect', 'verbose': 'Max. input voltage not detected'},
                        {'col': 'summary', 'key': 'min_high_set', 'verbose': 'Min. output load supported'},
                        {'col': 'summary', 'key': 'max_zero_set', 'verbose': 'Max. output load not supported'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'Input voltage test',
                        'doc': 'During the input voltage test, the digital value is get for different voltages applied on the pin. It checks the high level threshold by going from a low to a high value.',
                        'type': 'table',
                        'point_names': 'input_voltage_detect',
                        'columns': [
                            {'key': 'v', 'verbose': 'Input voltage'},
                            {'key': 'r', 'verbose': 'Read back from device'},
                        ]
                    },{
                        'type': 'scatter',
                        'label': 'voltage vs. load',
                        'title': 'Output load test',
                        'doc': 'During the output load test, the output voltage is measured for different load values. It checks whether the output current is compliant with the specifications',
                        'point_names': 'output_load_test',
                        'xkey': 'l',
                        'ykey': 'v'
                    }
                ],
                'doc': 'Optional - you can explain here what does the test do. It will be used into the report :-)'
            }
        ]
    }
