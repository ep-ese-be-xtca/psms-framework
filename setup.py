from setuptools import setup, find_packages

setup(
    name="PSMSFramework",
    version="0.1",
    packages=find_packages(),
    python_requires='>=3.4',

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=["flask","gevent","gitpython","docutils","graphviz","psycopg2", "sphinxcontrib-napoleon", "six", "eventhandler"],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        'PSMSFramework': ["templates/*", "static/css/*", "static/src/*", "static/src/format_styles/*"],
    },

    # metadata to display on PyPI
    author="Julian Mendez",
    author_email="julian.mendez@cern.ch",
    description="PSMS Framework package",
    keywords="PSMS",
    url="http://psms-template.web.cern.ch/",   # project home page, if any
    project_urls={
        "Bug Tracker": "https://bugs.example.com/HelloWorld/",
        "Documentation": "https://docs.example.com/HelloWorld/",
        "Source Code": "https://code.example.com/HelloWorld/",
    },
    classifiers=[
        "License :: OSI Approved :: Python Software Foundation License"
    ]

    # could also include long_description, download_url, etc.
)