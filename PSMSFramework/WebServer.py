import threading
import sys
import os
import git
import json
import subprocess
import importlib
import re
import traceback
import socket

from flask import Flask
from flask import render_template
from flask import request
from flask import send_file

from gevent import pywsgi
from gevent.pool import Pool


import PSMSFramework.sysapi as sysapi

class WebServer(threading.Thread):
    '''
    PSMSFramework webserver class runs the flask application.

    The init function initialize the Flask application and accept the following parameters:

    Args:
        title: Set the web application title
        worker: link to the test/debug function runner class
        git_root: set the root path of the git repository. Default = current folder
        test_root: set the root path of the test scripts. Default = curent folder

    The Flask server declares the following routes:

    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | Route           | Method     | Function                                                                    | Description                                                                |
    +=================+============+=============================================================================+============================================================================+
    | /               | GET        | `index <#PSMSFramework.WebServer.WebServer.index>`_                         | returns the main html page                                                 |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /version        | GET        | `get_version <#PSMSFramework.WebServer.WebServer.get_version>`_             | returns the local and server commit ids allowing to know whether an update |
    |                 |            |                                                                             | is available [status error when the project is not a git repo.]            |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /update         | GET        | `update_version <#PSMSFramework.WebServer.WebServer.update_version>`_       | triggers an update of the tester using git commands                        |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /list           | GET        | `get_function_list <#PSMSFramework.WebServer.WebServer.get_function_list>`_ | list the debug and test functions                                          |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /get_param_list | POST       | `get_param_list <#PSMSFramework.WebServer.WebServer.get_param_list>`_       | list all of the input parameters of a function                             |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /run            | POST       | `runfn <#PSMSFramework.WebServer.WebServer.runfn>`_                         | runs a specific debug or test function                                     |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /status         | GET        | `status <#PSMSFramework.WebServer.WebServer.status>`_                       | returns the current execution status                                       |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdout         | GET        | `stdout <#PSMSFramework.WebServer.WebServer.stdout>`_                       | returns the stdout output                                                  |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdin          | GET        | `get_stdin_req <#PSMSFramework.WebServer.WebServer.get_stdin_req>`_         | returns whether an input is required or not                                |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdin          | POST       | `send_stdin <#PSMSFramework.WebServer.WebServer.send_stdin>`_               | sends user data to server                                                  |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stop           | GET        | `stop_running <#PSMSFramework.WebServer.WebServer.stop_running>`_           | stop on-going action                                                       |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /files          | GET        | `list_files <#PSMSFramework.WebServer.WebServer.list_files>`_               | List exported files                                                        |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+

    The template and static folders are linked to the package **/template** and **/static** folders.
    Finally, it also implements a `run <#PSMSFramework.WebServer.WebServer.run>`_  and a `kill <#PSMSFramework.WebServer.WebServer.kill>`_ method allowing to start/stop the server from the main thread.

    **Methods description:**

    The chapters below details all of the methods implemented in the WebServer class.

    '''

    def __init__(self, title, worker, git_root='.', tests_root='.', export_root='', port=8000):
        script_route = os.path.abspath(os.path.dirname(__file__))
        template_dir = os.path.join(script_route, 'templates')
        static_dir = os.path.join(script_route, 'static')

        self.application = Flask(__name__, template_folder=template_dir, static_folder=static_dir)
        self.application.config['TEMPLATES_AUTO_RELOAD'] = True
        self.application.secret_key = 'G8dsr58huffffgdsdgj002'

        self.application.add_url_rule('/', 'index', self.index, methods=['GET'])
        self.application.add_url_rule('/version', 'get_version', self.get_version, methods=['GET'])
        self.application.add_url_rule('/update', 'update_version', self.update_version, methods=['GET'])
        self.application.add_url_rule('/list', 'get_function_list', self.get_function_list, methods=['GET'])
        self.application.add_url_rule('/get_param_list', 'get_param_list', self.get_param_list, methods=['POST'])
        self.application.add_url_rule('/run', 'run', self.runfn, methods=['POST'])
        self.application.add_url_rule('/status', 'status', self.status, methods=['GET'])
        self.application.add_url_rule('/stdout', 'stdout', self.stdout, methods=['GET'])
        self.application.add_url_rule('/stdin', 'get_stdin_req', self.get_stdin_req, methods=['GET'])
        self.application.add_url_rule('/stdin', 'send_stdin', self.send_stdin, methods=['POST'])
        self.application.add_url_rule('/stop', 'stop_running', self.stop_running, methods=['GET'])
        self.application.add_url_rule('/files', 'list_files', self.list_files, methods=['GET'])
        self.application.add_url_rule('/file/<filename>', 'get_file', self.get_file, methods=['GET'])

        self.title = title
        self.worker = worker
        self.port = port

        self.git_root = git_root
        self.tests_root = tests_root
        self.export_root = os.path.join(os.getcwd(), export_root)

        super().__init__()

    def stop_running(self):
        return json.dumps({'status': self.worker.stop()})

    def get_file(self, filename):
        if not os.path.exists(os.path.join(self.export_root, filename)) or not os.path.isfile(os.path.join(self.export_root, filename)):
            abort(404)

        return send_file(os.path.join(self.export_root, filename))

    def list_files(self):
        if self.export_root == '':
            return json.dumps({'status': -1, 'msg': 'Exported root path is not set'})

        if not os.path.exists(self.export_root):
            return json.dumps({'status': -2, 'msg': 'Exported root path does not exist'})

        onlyfiles = [f for f in os.listdir(self.export_root) if os.path.isfile(os.path.join(self.export_root, f))]

        return json.dumps({'status': 0, 'files': onlyfiles})

    def index(self):
        '''
        The index route returns the index.html template with the title set as an input parameters

        Returns:
            .. code-block:: python

                render_template(
                        'index.html',
                        title=self.title,
                        cleantitle=cleantitle
                    )
        '''
        cleanr = re.compile('<.*?>')
        cleantitle = re.sub(cleanr, '', self.title)

        if self.export_root != '':
            exportedFiles = True
        else:
            exportedFiles = False

        return render_template('index.html', title=self.title, cleantitle=cleantitle, exportedFiles=exportedFiles)


    def get_version(self):
        '''
        The get_version functions initialize a git repo variable from the git_root folder and get
        the local commit SHA. Then it fetches the repo to get the latest server information,
        get the head commit SHA and compare both to know whether the tester can be updated or
        not.

        Returns:
            .. code-block:: javascript

                /** When git repository exists */
                {
                    'status': 0,
                    'available': (sever_commit.hexsha != local.hexsha),
                    'server': sever_commit.hexsha,
                    'local': local.hexsha
                }


                /** When tester is not a git repository */
                {
                    'status': -1,
                    'repo': self.git_root,
                    'msg': str(e)
                }

        '''
        try:
            repo = git.Repo(self.git_root)
        except Exception as e:
            return json.dumps({'status': -1, 'repo': self.git_root, 'msg': str(e)})

        local = repo.commit()

        if len(repo.remotes.origin.fetch()):
            sever_commit = repo.remotes.origin.fetch()[0].commit
        else:
            return json.dumps({'status': -1, 'msg': 'Server connection failed', 'repo': self.git_root, 'local': local.hexsha})

        return json.dumps({'status': 0, 'available': (sever_commit.hexsha != local.hexsha), 'server': sever_commit.hexsha, 'local': local.hexsha})

    def update_version(self):
        '''
        The update function pull the latest version of the git repository, checks the requirements file and install dependancies if they changed.
        In case of failure during a package installation, it automatically rollback to the previous version.

        Returns:
            .. code-block:: javascript

                /** When succeed */
                {
                    'status': 0
                }

                /** When a dependency installation fails */
                {
                    'status': -1,
                    'msg': exc.output
                }

        '''
        repo = git.Repo(self.git_root)

        #Store current commit (used to check difference on requirements.txt file)
        prev_version = repo.commit()

        #Pull latest version
        o = repo.remotes.origin
        o.pull()

        #Get new commit
        curr_version = repo.commit()

        #List changed files
        req_file_changed = []
        diff = prev_version.diff(curr_version)

        for x in diff:
            if x.a_blob and x.a_blob.path not in req_file_changed:
                if 'requirements.txt' in x.a_blob.path:
                    req_file_changed.append(x.a_blob.path)

            if x.b_blob is not None and x.b_blob.path not in req_file_changed:
                if 'requirements.txt' in x.b_blob.path:
                    req_file_changed.append(x.b_blob.path)

        #Install packages if needed
        for f in req_file_changed:
            try:
                output = subprocess.check_output(
                    [sys.executable, "-m", "pip", "install", "-r", '{}/{}'.format(self.git_root, f).replace('//','/')],
                    stderr=subprocess.STDOUT, shell=True, timeout=3,
                    universal_newlines=True)
            except subprocess.CalledProcessError as exc:
                #In case of failure, go back to previous commit
                repo.git.reset('--hard',prev_version.hexsha)

                return json.dumps({'status':-1, 'msg': exc.output})

        return json.dumps({'status':0})

    def get_function_list(self):
        '''
        Import all of the files located into the test_root folder and subfolders to get all of the debug and test function registered.

        Returns:
            .. code-block:: javascript

                {
                    'tests': [
                            {
                                'package': test_fn_pkg,
                                'name': test_fn_verbose_name,
                                'function': test_fn_name
                            }, ...
                        ]
                    'debug': [
                            {
                                'package': dbg_fn_pkg,
                                'name': dbg_fn_verbose_name,
                                'function': dbg_fn_name
                            }, ...
                        ],
                    'errors': [
                            {
                                'package': package_name,
                                'error': title,
                                'traceback': traceback
                            }, ...
                        ]
                }

        '''
        fns = sysapi.get_function_list(self.tests_root)
        functions = {'tests': [], 'debug': [], 'errors': fns['errors']}

        for f in fns['tests']:
            functions['tests'].append({'package': f['package'], 'name': f['name'], 'function': f['function'].__name__})
        for f in fns['debug']:
            functions['debug'].append({'package': f['package'], 'name': f['name'], 'function': f['function'].__name__})

        return json.dumps(functions)

    def kill(self):
        '''
        The kill function allows stopping the webserver from the main threading
        '''
        print('[Info] Stop webserver...')
        self.server.stop()

    def status(self):
        '''
        Called on '/status' route:

        The status function returns the current test/debug function runner status.

        Returns:
            .. code-block:: javascript

                /** When idle - no function was called */
                {
                    'status': -2,
                    'msg': 'No function called'
                }

                /** Wait for running */
                {
                    'status': 2,
                    'fn': function_name,
                    'p': 0,
                    'type': type,
                        // '0': test
                        // '1': debug
                    'name': name
                        // Test/debug function verbose name
                }

                /** Is running */
                {
                    'status': 1,
                    'fn': function_name,
                    'p': progress,
                        // current progress value
                    'type': type,
                        // '0': test
                        // '1': debug
                    'name': name
                        // Test/debug function verbose name
                }

                /** Successfully finished */
                {
                    'status': 0,
                    'fn': function_name,
                    'ret': ret,
                        // Dict. returned by the function
                    'p': 100,
                    'type': type,
                        // '0': test
                        // '1': debug
                    'doc': function_doc,
                        // from file or function docstring
                    'name': name
                        // Test/debug function verbose name
                }

                /** Finished with exception */
                {
                    'status': -1,
                    'fn': self.fn.__name__,
                    'error': str(e),
                        // Error title
                    'traceback': err,
                        // Traceback details
                    'p': 100,
                    'type': type,
                        // '0': test
                        // '1': debug
                    'doc': function_doc,
                        // from file or function docstring
                    'name': name
                        // Test/debug function verbose name
                }
        '''
        return json.dumps(self.worker.status())

    def stdout(self):
        '''
        The stdout function get the current stdout value from the function runner.

        Returns:
            .. code-block:: javascript

                {
                    'status': 0,
                    'msg' stdout
                }
        '''
        return json.dumps({'status': 0, 'data': self.worker.get_stdout()})

    def get_stdin_req(self):
        '''
        The get_stdin_req function checks whether the runner wait for a user input.

        Returns:
            .. code-block:: javascript

                /** No user input requested */
                {
                    'status': -1
                }

                /** Waits for an input */
                {
                    'status': 0,
                    'name': param_name,
                    'doc': description
                }
        '''
        return json.dumps(self.worker.is_waiting_for_stdin())

    def send_stdin(self):
        '''
        The send_stdin receives the user input from the http request body and pass it to the runner.

        Args:
            JSON:

                .. code-block:: javascript

                    {
                        'stdin': user_input
                    }

        Returns:
            .. code-block:: javascript

                /** No user input requested */
                {
                    'status': 0
                }
        '''
        data = request.json
        try:
            self.worker.set_stdin(data['stdin'])
        except Exception as e:
            return json.dumps({'status': -1, 'msg': str(e)})

        return json.dumps({'status': 0})

    def runfn(self):
        '''
        The runfn function receives a JSON body from the HTTP request describing the debug/test function
        to be ran.

        Args:
            JSON:

                .. code-block:: javascript

                    {
                        'pkg': function_package,
                        'fn': function_name,
                        'params': param_dict
                    }

        Returns:
            .. code-block:: javascript

                /** When function is added to queue */
                {
                    'status': 0
                }

                /** When function a function is already running */
                {
                    'status': -1,
                    'srt': 'Runtime error: a function is already running',
                    'traceback': traceback
                }

                /** In case of wrong parameter */
                {
                    'status': -1,
                    'srt': 'Param ... is not set but required',
                    'traceback': traceback
                }
        '''
        data = request.json

        d_package = data['pkg']
        d_function = data['fn']
        d_params = data['params']

        return json.dumps(sysapi.run(d_package, d_function, d_params))

    def get_param_list(self):
        '''
        The get_param_list list all of the parameters required to run a specific debug/test function.

        Args:
            JSON:

                .. code-block:: javascript

                    {
                        'p': function_package,
                        'f': function_name
                    }

        Returns:
            .. code-block:: javascript

                /** When succeed */
                {
                    'status': 0,
                    'params': array_of_param_names
                }

                /** When the function import fails */
                {
                    'status': -1,
                    'error': import_error_msg,
                    'traceback': traceback
                }

                /** When the function is not found */
                {
                    'status': -2
                }
        '''
        data = request.json

        package = data['p']
        function = data['f']

        tmp = sysapi.list_fn_params(package, function)
        if tmp['status'] != 0:
            return json.dumps(tmp)

        return json.dumps({'status': 0, 'params': tmp['params']})

    def run(self):
        '''
        The run function starts the Flask application using the gevent WSGIServer. It spawn it over 5 workers to
        make the server more responsive.
        '''
        try:
            self.err = None

            print('[Info] Start webserver...')
            hostname = socket.gethostname()
            IPAddr = socket.gethostbyname(hostname)

            print("[Info] URL: http://{}:{}".format(hostname, self.port))

            self.server = pywsgi.WSGIServer(('0.0.0.0', self.port), self.application, spawn=5 and Pool(5) or 'default', log=None)
            self.server.serve_forever()

        except Exception as e:
            print('EXCEPTION: {}'.format(e))
            self.err = str(e)