from io import StringIO
import time
import traceback
import importlib
import inspect
import os
import sys

TEST_FUNCTIONS = []
DEBUG_FUNCTIONS = []

SET_NO_VERBOSITY = 0x00
SET_PROGRESS_VERBOSITY = 0x01

class configuration:
    def __init__(self):
        self.WORKER_CLASS = None
        self.VERBOSE_LEVEL = 0xFF
        self.fnlist = {'tests': [], 'debug': [], 'errors': []}

    def set_verbosity(self, v):
        self.VERBOSE_LEVEL = v

    def set_worker_class(self, v):
        self.WORKER_CLASS = v

    def get_verbosity(self):
        return self.VERBOSE_LEVEL

    def get_worker_class(self):
        return self.WORKER_CLASS

    def set_function_list(self, fnlist):
        self.fnlist = fnlist

    def get_function_list(self):
        return self.fnlist

CONFIG_OBJ = configuration()

def run(package, function, args):
    global CONFIG_OBJ

    WORKER_CLASS = CONFIG_OBJ.get_worker_class()

    try:
        list_fn_p = list_fn_params(package, function)
        if list_fn_p['status'] != 0:
            raise Exception('Error: function param listing failed')

        f_params = list_fn_p['params']
        custom_args = {}

        try:
            for p in f_params:
                if p not in args or not args[p]:
                    raise Exception('Param {} is not set but required'.format(p))

                custom_args[p] = args[p]
        except:
            pass

        if len(f_params) != len(custom_args):
            raise Exception('Params are not set but required {}'.format(f_params))

        WORKER_CLASS.push(list_fn_p['fn'], custom_args, list_fn_p['type'], list_fn_p['name'])
        CONFIG_OBJ.set_worker_class(WORKER_CLASS)

        return {'status': 0}

    except Exception as e:
        return {'status': -1, 'error': str(e), 'traceback': traceback.format_exc()}

def set_verbosity(verbosity):
    global CONFIG_OBJ
    CONFIG_OBJ.set_verbosity(verbosity)

def set_job_progress(p):
    global CONFIG_OBJ

    WORKER_CLASS = CONFIG_OBJ.get_worker_class()
    VERBOSE_LEVEL = CONFIG_OBJ.get_verbosity()

    if WORKER_CLASS is None:
        if (VERBOSE_LEVEL & 0x01):
            print('[Progress] {}%'.format(p))
        return

    WORKER_CLASS.set_progress(p, inspect.stack()[1][3])

def get_data(param_name, param_desc=''):
    global CONFIG_OBJ

    WORKER_CLASS = CONFIG_OBJ.get_worker_class()

    if WORKER_CLASS is None:
        return raw_input(param_name+": ")

    WORKER_CLASS.get_data(param_name, param_desc, 0)
    CONFIG_OBJ.set_worker_class(WORKER_CLASS)

    print('[Pause] Wait for data from web interface: {}'.format(param_name))
    while CONFIG_OBJ.get_worker_class().get_stdin() is None:
        time.sleep(0.100)

    return CONFIG_OBJ.get_worker_class().get_stdin()

def get_yesno(param_name, param_desc=''):
    global CONFIG_OBJ

    WORKER_CLASS = CONFIG_OBJ.get_worker_class()

    if WORKER_CLASS is None:
        return raw_input(param_name+": ")

    WORKER_CLASS.get_data(param_name, param_desc, type=1)
    CONFIG_OBJ.set_worker_class(WORKER_CLASS)

    print('[Pause] Wait for data from web interface: {}'.format(param_name))
    while CONFIG_OBJ.get_worker_class().get_stdin() is None:
        time.sleep(0.100)

    v = CONFIG_OBJ.get_worker_class().get_stdin()

    return False if v == 'n' else True

def clear():
    TEST_FUNCTIONS.clear()
    DEBUG_FUNCTIONS.clear()

def get_function_list(tests_root):
    global CONFIG_OBJ

    functions = {'tests': [], 'debug': [], 'errors': []}
    for (dirpath, dirnames, filenames) in os.walk(tests_root):
        for f in [os.path.join(dirpath, file) for file in filenames]:
            if f[-3:] == '.py':
                package = f.replace('./','')[:-3]
                package = package.replace('.\\','')
                package = package.replace('/','.')
                package = package.replace('\\','.')

                try:
                    command_module = importlib.import_module(package)
                    TEST_FUNCTIONS.clear()
                    DEBUG_FUNCTIONS.clear()
                    importlib.reload(command_module)

                    testlist = getTestList()

                    for t in testlist:
                        functions['tests'].append({'package': package, 'name': t['name'], 'function': t['function']})

                    dbglist = getDebugList()

                    for t in dbglist:
                        functions['debug'].append({'package': package, 'name': t['name'], 'function': t['function']})

                except Exception as e:
                    functions['errors'].append({'package': package, 'error': str(e), 'traceback': traceback.format_exc()})

    CONFIG_OBJ.set_function_list(functions)

    return functions


def register_test(name):
    def wrapper(f):
        TEST_FUNCTIONS.append({'name': name, 'function': f})
        return f
    return wrapper

def getTestList():
    return TEST_FUNCTIONS

def register_debug(name):
    def wrapper(f):
        DEBUG_FUNCTIONS.append({'name': name, 'function': f})
        return f
    return wrapper

def getDebugList():
    return DEBUG_FUNCTIONS


def get_function_desc(function_name):
    global CONFIG_OBJ

    functions = CONFIG_OBJ.get_function_list()
    try:
        testlist = functions['tests']
        for t in testlist:
            if t['function'].__name__ == function_name:
                t['type'] = 0
                return t

        dbglist = functions['debug']
        for t in dbglist:
            if t['function'].__name__ == function_name:
                t['type'] = 1
                return t

    except Exception as e:
        return None

    return None

def list_fn_params(package, function):

    try:
        t = get_function_desc(function)
        params_tmp = t['function'].__code__.co_varnames[0:(t['function'].__code__.co_argcount)]

        params = []
        for p in params_tmp:
            if p != 'self':
                params.append(p)

        return {'status': 0, 'fn': t['function'], 'params': params, 'name': t['name'], 'type': t['type']}

    except Exception as e:
        return {'status': -1, 'error': str(e), 'traceback': traceback.format_exc()}

    return {'status': -2}