var update = function(button_id, update_callback){

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;
    var btn_id = button_id;
    var updt_cback = update_callback;

    /*
     * Constructor
     */
    this.construct = function(){
        window.setInterval(function(){
            check_for_update();
        }, 5000);
        check_for_update();

        $('body').append(''+
            '<div id="update-done-modal" class="w3-modal">'+
            '  <div class="w3-modal-content">'+
            '    <div class="w3-container w3-padding">'+
            '        <span onclick="document.getElementById(\'update-done-modal\').style.display=\'none\'"  class="w3-button w3-display-topright">&times;</span>'+
            '        <h4>Update</h4>'+
            '        <div class="w3-row">'+
            '            <div id="update-msg" style="text-align: justify">'+
            '            </div>'+
            '        </div>'+
            '    </div>'+
            '  </div>'+
            '</div>');

        $('#'+btn_id).click(function(){
            $('#'+btn_id).attr("disabled", true);
            $('#'+btn_id).html('In progress...');

            fetch('/update', {
                method: 'GET',
                credentials: 'include'
            })

            .then( (response) => {

                $('#'+btn_id).removeAttr("disabled");
                $('#'+btn_id).html('<b>Update</b>');

                if(!response.ok){
                    printError('<b>Server error:</b> HTTP '+response.status);
                    return null;
                }
                return response.json()
            })

            .then( (json) => {
                if(json == null) return;

                if(json['status'] == -1){
                    printError('<b>Package installation error:</b> <pre style="margin-top:16px"><code>'+json['msg']+'</code></pre><p><b>Warning:</b> Automatically reverted to the previous commit</p>');
                    return null;
                }
                else if(json['status'] < 0){
                    printError('<b>Server error:</b> '+json['error']+'<pre style="margin-top:16px"><code>'+json['traceback']+'</code></pre>')
                    return null;
                }else{
                    $('#'+btn_id).hide();
                    printSuccess('Repository has been succesfully updated');
                    updt_cback();
                }
            })
        });
    };

    var check_for_update = function(){
        fetch('/version', {
            method: 'GET',
            credentials: 'include'
        })

        .then( (response) => {
            if(!response.ok){
                $('#'+btn_id).hide();
                return null;
            }
            return response.json()
        })

        .then( (json) => {
            if(json == null){
                $('#'+btn_id).fadeOut(200);
            }
            else if(json != null && json['status'] != 0){
                $('#'+btn_id).fadeOut(200);
                //printError(json['msg']);
            }else if(json != null && json['available'] == true){
                $('#'+btn_id).fadeIn(200);
            }else{
                $('#'+btn_id).fadeOut(200);
            }
        })
    };

    var printError = function(msg){
        $('#update-msg').html('<div class="w3-red w3-container w3-card" style="padding-top: 16px; padding-bottom: 16px"><b>Error:</b> '+msg+'</div>');
        $('#update-done-modal').fadeIn(200);
    }

    var printSuccess = function(msg){
        $('#update-msg').html('<div class="w3-green w3-container w3-card" style="padding-top: 16px; padding-bottom: 16px"><b>Success:</b> '+msg+'</div>');
        $('#update-done-modal').fadeIn(200);
    }

    this.construct();

};
