var testRunner = function(progress_node){

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;
    var p_node = progress_node;
    var cback = undefined;

    /*
     * Constructor
     */
    this.construct = function(){
        $('#'+p_node).html(''+
               '<div class="w3-col l12 w3-container">'+
               '    <div class="w3-light-grey w3-round" style="height:20px">'+
               '        <div id="progress-bar" class="w3-container w3-round w3-blue" style="width:25%; height: 20px"></div>'+
               '        <div id="progress-text" style="position: relative; text-align: center; margin-top: -20px">25%</div>'+
               '    </div>'+
               '</div>');
        $('#'+p_node).hide();
    };

    this.run = function(pkg, fn, params, cback){

        this.set_percent(0, fn);
        $('#'+p_node).show();

        fetch('/run', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                pkg: pkg,
                fn: fn,
                params: params
            }),
        })

        .then( (response) => {
            if(!response.ok){
                console.log('<b>Server error:</b> HTTP '+response.status);
                return null;
            }
            return response.json()
        })

        .then( (json) => {
            if(json == null){ cback(-2); return; }

            if(json['status'] == -1){
                $('#'+p_node).hide();

                $('#response-node').html('<div style="text-align: justify" class="w3-red w3-container w3-card"><p><b>Server error:</b> '+json['error']+'</p><pre style="margin-top:16px"><code>'+json['traceback']+'</pre></code></div>');
                $('#response-node').slideDown(200);

                cback(-1);
                return;
            }

            cback(0);
        })
    };

    this.set_percent = function(percent, function_name){
      $('#progress-text').html(function_name+' ['+percent+'%]');
      $('#progress-bar').css('width',percent+'%');
    };

    this.construct();

};