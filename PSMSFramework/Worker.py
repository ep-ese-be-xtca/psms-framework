from queue import Queue

import logging
import sys
import traceback
import PSMSFramework.sysapi as sysapi
import PSMSFramework.PSQLWriter as PSQLWriter
import importlib
import time
import os

import queue
from threading import Thread
from eventhandler import EventHandler

import signal
import base64
import graphviz
import textwrap
import six
from six import text_type, u

from sphinxcontrib import napoleon

from docutils.core import publish_string
from docutils.writers.html4css1 import Writer, HTMLTranslator
from docutils.parsers.rst import Directive, directives
from docutils import nodes
from io import StringIO

class StdoutHandler(StringIO):

    def __init__(self, worker):
        StringIO.__init__(self)
        self.terminal = sys.stdout
        self.worker = worker

    def write(self, record):
        self.terminal.write(record)
        self.worker.print_stdout(record)

class runKill(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass

class FlowChartDirective(Directive):
    '''This `Directive` class tells the ReST parser what to do with the text it
    encounters -- parse the input, perhaps, and return a list of node objects.
    Here, usage of a single required argument is shown.
    See examples in docutils.parsers.rst.directives.*
    '''
    required_arguments = 0
    optional_arguments = 0
    has_content = True
    def run(self):
        #self.content
        try:
            content = '\n'.join(self.content)
            src = graphviz.Source(content)
            output = src.pipe(format='png')

            data_url_filetype = 'png'
            alt = 'flowchart'
            output = base64.b64encode(output).decode()
            data_path = "data:image/%s;base64,%s" % (data_url_filetype, output)
            attrs = []
            attrs.append('src="%s"' % data_path)
            attrs.append('alt="%s"' % alt)
            img = '<p style="text-align: center"><img %s /></p>' % ' '.join(attrs)
        except Exception as e:
            img = '<p style="text-align: center; color: orangee">Graphviz is not supported: {}</p>'.format(str(e))

        return [nodes.raw('', img, format='html')]

class Worker():
    def __init__(self, host=None, username=None, password=None, port=None, dbname=None):
        self.fn = None
        self.sts = {'status': -2, 'msg': 'No function called'}
        self.args = {}
        self.type = ''
        self.name = ''
        self.progress = 0
        self.stdout = ''
        self.stdin = None
        self.stdin_req = None
        self.stdin_doc = None
        self.is_off = False
        self.wid = 0
        self.last_stdout = 0
        self.is_running = False

        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.dbname = dbname

        root_logger = logging.getLogger()
        log_formatter = logging.Formatter('[%(levelname)-7s] %(message)s')
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(log_formatter)
        console_handler.setLevel(logging.INFO)
        root_logger.addHandler(console_handler)

    def get_event_handler(self):
        return self.event_handler

    def isOff(self):
        return self.is_off

    def shutdown(self):
        self.is_off = True

    def push(self, fn, args, type, name):
        if self.fn is not None:
            raise Exception('Runtime error: a function is already running');

        self.fn = fn
        self.args = args
        self.type = type
        self.name = name
        self.stdout = ''
        self.last_stdout = time.time()

        self.stdin = None
        self.stdin_req = None
        self.stdin_doc = None

        self.wid = time.time()

        self.sts = {
                'status': 2,
                'wid': self.wid,
                'fn': self.fn.__name__,
                'p': 0,
                'type': type,
                'name': self.name
            }

    def set_progress(self, p, name):
        self.sts['p'] = p

        t = sysapi.get_function_desc(name)
        if t is None:
            self.sts['name'] = name
        else:
            self.sts['name'] = t['name']

    def status(self):
        return self.sts

    def print_stdout(self, msg):
        self.last_stdout = time.time()
        self.stdout += msg

    def get_stdout(self):
        return {'msg': self.stdout, 'time': self.last_stdout}

    def get_data(self, name, doc, type=0):
        '''
            stdin_type:
                0 -> text input
                1 -> yes/no
        '''
        self.stdin = None
        self.stdin_type = type
        self.stdin_req = name
        self.stdin_doc = doc

    def get_stdin(self):
        return self.stdin

    def set_stdin(self, msg):
        if self.stdin_req is None:
            raise Exception('STDIN param was already set')

        self.stdin = msg
        self.stdin_req = None
        self.stdin_doc = None

    def is_waiting_for_stdin(self):
        if self.stdin_req is None:
            return {'status': -1}

        else:
            return {'status': 0, 'type': self.stdin_type, 'name': self.stdin_req, 'doc': self.stdin_doc}

    def stop(self):
        #signal.raise_signal(signal.SIGUSR1)
        if self.is_running:
            os.kill(os.getpid(), signal.SIGUSR1)
            return 0
        else:
            return -1

    def service_shutdown(self, signum, frame):
        raise runKill

    def run(self):
        if self.fn is None:
            return

        self.sts = {
                'status': 1,
                'wid': self.wid,
                'fn': self.fn.__name__,
                'p': 0,
                'type': self.type,
                'name': self.name
            }

        directives.register_directive('graphviz', FlowChartDirective)

        try:
            if self.type == 1:
                mydocstring = publish_string(self.fn.__doc__, writer_name='html').decode('utf-8') #.replace('\n',' ')
            else:
                m = importlib.import_module(self.fn.__module__)
                #c = getattr(m)

                mydocstring = publish_string(m.__doc__, writer_name='html').decode('utf-8')

        except Exception as e:
            mydocstring = str(e)

        tmp_stdout = sys.stdout
        tmp_stderr = sys.stderr
        sys.stdout = StdoutHandler(self)
        sys.stderr = StdoutHandler(self)

        try:
            #que = queue.Queue()
            #
            #self.event_handler = EventHandler('kill')
            #self.runthread = Thread(target=lambda q, arg1: q.put(self.fn(**arg1)), args=(que, self.args))
            #self.runthread.start()
            #self.runthread.join()
            #self.runthread = None
            #self.event_handler = None
            #ret = que.get()

            signal.signal(signal.SIGUSR1, self.service_shutdown)
            try:
                self.is_running = True
                ret = self.fn(**self.args)
            except runKill:
                print('[End] Program stopped by user')
                self.is_running = False
                self.sts = {
                        'status': -1,
                        'wid': self.wid,
                        'fn': self.fn.__name__,
                        'error': 'Program stopped by user',
                        'traceback': '',
                        'p': 100,
                        'type': self.type,
                        'doc': mydocstring,
                        'name': self.name
                    }

                sys.stdout = tmp_stdout
                sys.stderr = tmp_stderr

                self.fn = None
                return

            self.is_running = False

            ''' For test type function, push the result into the databse'''
            try:
                dbStatus = 0
                dbErr = ''
                dbErrTback = ''

                if self.type == 0 and self.host is not None:

                    #Init: connect to db
                    db = PSQLWriter.PSQLWriter(self.host,self.username,self.password,self.port,self.dbname)

                    #1/ Check for device/batch information
                    if 'device' not in ret:
                        raise Exception("'device' key is not returned by the test function")

                    if 'batch' not in ret['device']:
                        raise Exception("'batch' key is not returned by the test function (shall be in device node)")

                    try:
                        batchName = ret['device']['batch']['name']
                    except:
                        batchName = None

                    try:
                        batchDesc = ret['device']['batch']['details']
                    except:
                        batchDesc = None

                    b = db.getBatch(batchName=batchName, batchDesc=batchDesc)
                    if len(b) > 1:
                        raise Exception('More than one batch has been found (name: {}, details: {}) - {} found'.format(batchName, batchDesc, len(b)))

                    batchId = b[0]['batchId']

                    try:
                        deviceName = ret['device']['name']
                    except:
                        deviceName = ''

                    try:
                        deviceDesc = ret['device']['details']
                    except:
                        deviceDesc = {}

                    try:
                        d = db.getDevice(deviceName=deviceName, batchId=batchId, deviceDesc=deviceDesc)
                        if len(d) > 1:
                            raise Exception('More than one device has been found (name: {}, batch: {}, details: {}) - {} found'.format(deviceName, batchId, deviceDesc, len(d)))

                        deviceId = d[0]['deviceId']

                    except PSQLWriter.TestResException:
                        deviceId = db.addDevice(deviceName=deviceName, batchId=batchId, deviceDesc=deviceDesc)

                    #Check for all of the subtests?
                    if 'subtests' not in ret or len(ret['subtests']) == 0:
                        raise Exception('The test shall contain at least one subtest')

                    for st in ret['subtests']:
                        if 'name' not in st:
                            raise Exception("Subtests shall contain the 'name' key")

                        if 'pass' not in st:
                            raise Exception("Subtest {} shall contain the 'pass' key".format(st['name']))

                        if 'test_type_name' not in st:
                            raise Exception("Subtest {} shall contain the 'test_type_name' key".format(st['name']))

                        tt = db.getTestType(testTypeName=st['test_type_name'])
                        if len(tt) > 1:
                            raise Exception('More than one test_type has been found (test_type_name: {}) - {} found'.format(st['test_type_name'], tt))

                        st['testTypeId'] = tt[0]['testTypeId']

                    #Push test
                    if 'pass' not in ret:
                        raise Exception("'pass' key is not returned by the test function")

                    try:
                        testDescription = ret['details']
                    except:
                        testDescription = None

                    testId = db.addTest(deviceId = deviceId, testDescription = testDescription, passed=ret['pass'])

                    for st in ret['subtests']:
                        try:
                            summary = st['summary']
                        except:
                            summary = None
                        try:
                            details = st['details']
                        except:
                            details = None
                        try:
                            measurements = st['measurements']
                        except:
                            measurements = None

                        db.pushSubTest(testId = testId, testTypeId = st['testTypeId'], deviceId = deviceId, name = st['name'], summary = summary, details = details, measurements = measurements, passed = st['pass'])

                    db.disconnect()

            except Exception as e:
                try:
                    db.disconnect()
                except:
                    pass

                dbStatus = -1
                dbErrTback = traceback.format_exc()
                dbErr = str(e)

                print(dbErr)

            self.sts = {
                'status': 0,
                'wid': self.wid,
                'fn': self.fn.__name__,
                'ret': ret,
                'p': 100,
                'type': self.type,
                'doc': mydocstring,
                'name': self.name,
                'db': {
                    'status': dbStatus,
                    'error': dbErr,
                    'traceback': dbErrTback,
                }
            }

        except Exception as e:

            err = traceback.format_exc()

            print(err)

            self.sts = {
                'status': -1,
                'wid': self.wid,
                'fn': self.fn.__name__,
                'error': str(e),
                'traceback': err,
                'p': 100,
                'type': self.type,
                'doc': mydocstring,
                'name': self.name
            }

        sys.stdout = tmp_stdout
        sys.stderr = tmp_stderr

        self.fn = None